﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Compilation;
using System.Web.Routing;

namespace Rest_API
{
    public class ListPessoaRouteHandler : IRouteHandler
    {
        private const string virtualPath = "~/Endpoints/ListPessoa.ashx";

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return BuildManager.CreateInstanceFromVirtualPath(virtualPath, typeof(IHttpHandler)) as IHttpHandler;
        }
    }
}