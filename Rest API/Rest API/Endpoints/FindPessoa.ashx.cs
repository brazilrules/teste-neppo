﻿using Rest_API.DAO;
using Rest_API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rest_API.Endpoints
{
    /// <summary>
    /// Summary description for FindPerson
    /// </summary>
    public class FindPessoa : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string ErrorMessage;
            if (context.Request.HttpMethod == "GET")
            {
                DAOPessoa dao = new DAOPessoa();

                int id;
                if (Int32.TryParse(context.Request.Params["id"], out id)) {

                    Pessoa pessoa = (Pessoa)dao.Find(id);
                    
                    context.Response.ContentType = "application/json";
                    context.Response.Write(pessoa.ToJson());

                    return;
                }
                else
                {
                    ErrorMessage = "The id parameter must be an integer";
                }
            }
            else
            {
                ErrorMessage = "Invalid Method. This endpoint only accepts GET";
            }
            context.Response.ContentType = "text/html; charset=utf-8";
            context.Response.Write(ErrorMessage);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}