﻿using Rest_API.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rest_API.Endpoints
{
    /// <summary>
    /// Summary description for DeletePessoa
    /// </summary>
    public class DeletePessoa : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string responseMessage;
            if (context.Request.HttpMethod == "DELETE")
            {
                DAOPessoa dao = new DAOPessoa();

                int id;
                if (Int32.TryParse(context.Request.Params["id"], out id)) {

                    dao.Delete(id);
                    
                    responseMessage = "Pessoa deleted succesfully";

                }
                else
                {
                    responseMessage = "The id parameter must be an integer";
                }
            }
            else
            {
                responseMessage = "Invalid Method. This endpoint only accepts DELETE";
            }
            context.Response.ContentType = "text/html; charset=utf-8";
            context.Response.Write(responseMessage);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}