﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Rest_API.DAO;
using Rest_API.Entities;

namespace Rest_API.Endpoints
{
    /// <summary>
    /// Summary description for ListPessoa
    /// </summary>
    public class ListPessoa : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.HttpMethod == "GET")
            {
                DAOPessoa dao = new DAOPessoa();

                List<Entity> pessoas = dao.List();
                string jsonArray;
                if (pessoas.Count > 0)
                {
                    jsonArray = "[";
                    foreach (Entity entity in pessoas)
                    {
                        Pessoa pessoa = (Pessoa)entity;
                        jsonArray = $"{jsonArray}{pessoa.ToJson()},";
                    }
                    jsonArray = $"{jsonArray.Substring(0, jsonArray.Length - 1)}]";
                }
                else
                {
                    jsonArray = "[]";
                }

                context.Response.ContentType = "application/json";
                context.Response.Write(jsonArray);
            }
            else
            {
                context.Response.ContentType = "text/html; charset=utf-8";
                context.Response.Write("Invalid Method. This endpoint only accepts GET");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}