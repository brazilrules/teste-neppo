﻿using Rest_API.DAO;
using Rest_API.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Rest_API.Endpoints
{
    /// <summary>
    /// Summary description for PersistPessoa
    /// </summary>
    public class PersistPessoa : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string responseMessage;
            if (context.Request.HttpMethod == "POST")
            {
                try
                {
                    string json;
                    using (StreamReader reader = new StreamReader(context.Request.InputStream))
                    {
                        json = reader.ReadToEnd();
                    }
                    
                    DAOPessoa dao = new DAOPessoa();
                    Pessoa pessoa = new Pessoa(json);

                    pessoa.Url = context.Request.Url.ToString().Replace("PersistPessoa.ashx", "") + "FindPessoa.ashx?id=";

                    dao.Create(pessoa);

                    responseMessage = "Pessoa created successfully";
                }
                catch (Exception ex)
                {
                    responseMessage = ex.Message;
                }
            }
            else if (context.Request.HttpMethod == "PUT")
            {
                try
                {
                    string json;
                    using (StreamReader reader = new StreamReader(context.Request.InputStream))
                    {
                        json = reader.ReadToEnd();
                    }

                    DAOPessoa dao = new DAOPessoa();
                    Pessoa pessoa = new Pessoa(json);

                    dao.Update(pessoa);

                    responseMessage = "Pessoa updated successfully";
                }
                catch (Exception ex)
                {
                    responseMessage = ex.Message;
                }
            }
            else
            {
                responseMessage = "Invalid Method. This endpoint only accepts POST and PUT";
            }
            context.Response.ContentType = "text/html; charset=utf-8";
            context.Response.Write(responseMessage);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}