﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;

namespace Rest_API.Entities
{
    [DataContract]
    public class Pessoa : Entity
    {
        
        [DataMember(Name="nome")]
        public string Nome { get; set; }
        public DateTime? DataNascimento { get; set; }
        [DataMember(Name="identificacao")]
        public string Identificacao { get; set; }
        [DataMember(Name="sexo")]
        public bool? Sexo { get; set; }
        [DataMember(Name="endereco")]
        public string Endereco { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        [DataMember(Name="url")]
        public string Url { get; set; }

        [DataMember(Name = "dataNascimento")]
        public string JsonDataNascimento {
            get
            {
                return DataNascimento != null ? DataNascimento.Value.ToString("yyyy-MM-dd") : string.Empty;
            }

            set
            {
                if (value != null && value != string.Empty)
                {
                    DataNascimento = DateTime.ParseExact(value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                }
            }
        }

        [DataMember(Name = "createdAt")]
        public string JsonCreatedAt
        {
            get
            {
                return CreatedAt != null ? CreatedAt.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") : string.Empty;
            }

            set
            {
                if (value != null && value != string.Empty)
                {
                    CreatedAt = DateTime.ParseExact(value, "yyyy-MM-ddTHH:mm:ss.fffZ", CultureInfo.InvariantCulture);
                }
            }
        }

        [DataMember(Name = "updatedAt")]
        public string JsonUpdatedAt
        {
            get
            {
                return UpdatedAt != null ? UpdatedAt.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") : string.Empty;
            }

            set
            {
                if (value != null && value != string.Empty)
                {
                    UpdatedAt = DateTime.ParseExact(value, "yyyy-MM-ddTHH:mm:ss.fffZ", CultureInfo.InvariantCulture);
                }
            }
        }

        public Pessoa() { }

        public Pessoa(string json)
        {
            FromJson(json);
        } 

        public override string ToJson()
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Pessoa));
            MemoryStream stream = new MemoryStream();

            serializer.WriteObject(stream, this);

            stream.Position = 0;
            StreamReader reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }

        public override void FromJson(string json)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Pessoa));
            MemoryStream stream = new MemoryStream(Encoding.Unicode.GetBytes(json));

            Pessoa p = (Pessoa)serializer.ReadObject(stream);
            Id = p.Id;
            Nome = p.Nome;
            DataNascimento = p.DataNascimento;
            Identificacao = p.Identificacao;
            Sexo = p.Sexo;
            Endereco = p.Endereco;
            CreatedAt = p.CreatedAt;
            UpdatedAt = p.UpdatedAt;
            Url = p.Url;
        }
    }
}