﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace Rest_API.Entities
{
    [DataContract]
    public abstract class Entity
    {
        [DataMember(Name = "id")]
        public int? Id { get; set; }

        public abstract string ToJson();
        public abstract void FromJson(string json);
    }
}