﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Web;
using Rest_API.Entities;

namespace Rest_API.DAO
{
    public abstract class ADAO
    {
        private string dataBasePath = "{0}Database\\db.sqlite";
        private const string connectionString = "Data Source={0}; Version=3;";
        protected string Table { get; set; }

        public ADAO()
        {
            dataBasePath = String.Format(dataBasePath, HttpRuntime.AppDomainAppPath);
        }

        public List<Entity> List()
        {
            SQLiteConnection conn = new SQLiteConnection(String.Format(connectionString, dataBasePath));
            SQLiteDataAdapter da = new SQLiteDataAdapter($"SELECT * FROM {Table}", conn);
            DataTable dt = new DataTable();
            conn.Open();
            da.Fill(dt);
            conn.Close();
            return CreateList(dt);

        }

        public Entity Find(int? id)
        {
            SQLiteConnection conn = new SQLiteConnection(String.Format(connectionString, dataBasePath));
            SQLiteDataAdapter da = new SQLiteDataAdapter($"SELECT * FROM {Table} WHERE Id = {id}", conn);
            DataTable dt = new DataTable();
            conn.Open();
            da.Fill(dt);
            conn.Close();
            return CreateEntity(dt.Rows[0]);
        }

        public int? Create(Entity entity)
        {
            SQLiteConnection conn = new SQLiteConnection(String.Format(connectionString, dataBasePath));
            SQLiteDataAdapter da = new SQLiteDataAdapter("select id from pessoa ORDER BY ROWID DESC LIMIT 1", conn);
            SQLiteCommand cmd = new SQLiteCommand($"INSERT INTO {Table}{BuildCreateList()}", conn);
            DataTable dt = new DataTable();
            conn.Open();
            da.Fill(dt);
            cmd.Parameters.AddRange(BuildCreateParameters(entity, ((long)dt.Rows[0]["id"]) + 1)); 
            cmd.ExecuteNonQuery();
            int? id = (int?)conn.LastInsertRowId;
            conn.Close();
            return id;
        }

        public void Update(Entity entity)
        {
            if (entity.Id == null) throw new Exception("You need to pass the id to update");
            SQLiteConnection conn = new SQLiteConnection(String.Format(connectionString, dataBasePath));
            SQLiteCommand cmd = new SQLiteCommand($"UPDATE {Table} SET {BuildUpdateList()} WHERE Id = {entity.Id}", conn);
            cmd.Parameters.AddRange(BuildUpdateParameters(entity));
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void Delete(int? id)
        {
            SQLiteConnection conn = new SQLiteConnection(String.Format(connectionString, dataBasePath));
            SQLiteCommand cmd = new SQLiteCommand($"DELETE FROM {Table} WHERE Id = {id}", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        protected abstract List<Entity> CreateList(DataTable dt);
        protected abstract Entity CreateEntity(DataRow dr);
        protected abstract string BuildCreateList();
        protected abstract SQLiteParameter[] BuildCreateParameters(Entity entity, long lastId);
        protected abstract string BuildUpdateList();
        protected abstract SQLiteParameter[] BuildUpdateParameters(Entity entity);

    }
}