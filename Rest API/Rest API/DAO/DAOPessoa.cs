﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Web;
using Rest_API.Entities;
using System.Globalization;

namespace Rest_API.DAO
{
    public class DAOPessoa : ADAO
    {
        public DAOPessoa() : base()
        {
            Table = "pessoa";
        }

        protected override string BuildCreateList()
        {
            return "(nome, dataNascimento, identificacao, sexo, endereco, createdAt, url) VALUES(@nome, @dataNascimento, @identificacao, @sexo, @endereco, @createdAt, @url)";
        }

        protected override SQLiteParameter[] BuildCreateParameters(Entity entity, long lastId)
        {
            Pessoa p = (Pessoa)entity;
            p.CreatedAt = DateTime.Now;
            SQLiteParameter[] parameters = new SQLiteParameter[] {
                new SQLiteParameter("@nome", p.Nome),
                new SQLiteParameter("@dataNascimento", p.JsonDataNascimento),
                new SQLiteParameter("@identificacao", p.Identificacao),
                new SQLiteParameter("@sexo", LongFromBool(p.Sexo)),
                new SQLiteParameter("@endereco", p.Endereco),
                new SQLiteParameter("@createdAt", p.JsonCreatedAt),
                new SQLiteParameter("@url", p.Url + lastId)
            };
            return parameters;
        }

        protected override string BuildUpdateList()
        {
            return "nome = @nome, dataNascimento = @dataNascimento, identificacao = @identificacao, sexo = @sexo, endereco = @endereco, updatedAt = @updatedAt, url = @url";
        }

        protected override SQLiteParameter[] BuildUpdateParameters(Entity entity)
        {
            Pessoa p = (Pessoa)entity;
            p.UpdatedAt = DateTime.Now;
            SQLiteParameter[] parameters = new SQLiteParameter[] {
                new SQLiteParameter("@nome", p.Nome),
                new SQLiteParameter("@dataNascimento", p.JsonDataNascimento),
                new SQLiteParameter("@identificacao", p.Identificacao),
                new SQLiteParameter("@sexo", LongFromBool(p.Sexo)),
                new SQLiteParameter("@endereco", p.Endereco),
                new SQLiteParameter("@updatedAt", p.JsonUpdatedAt),
                new SQLiteParameter("@url", p.Url)
            };
            return parameters;
        }

        protected override Entity CreateEntity(DataRow dr)
        {
            int? id = !(dr["id"] is DBNull) ? (int?)((long)dr["id"]) : null;
            string nome = !(dr["nome"] is DBNull) ? dr["nome"].ToString() : null;
            string dataNascimento = !(dr["dataNascimento"] is DBNull) ? dr["dataNascimento"].ToString() : null;
            string identificacao = !(dr["identificacao"] is DBNull) ? dr["identificacao"].ToString() : null;
            bool? sexo = !(dr["sexo"] is DBNull) ? BoolFromLong((long)dr["sexo"]) : null;
            string endereco = !(dr["endereco"] is DBNull) ? dr["endereco"].ToString() : null;
            string createdAt = !(dr["createdAt"] is DBNull) ? dr["createdAt"].ToString() : null;
            string updatedAt = !(dr["updatedAt"] is DBNull) ? dr["updatedAt"].ToString() : null;
            string url = !(dr["url"] is DBNull) ? dr["url"].ToString() : null;

            return new Pessoa
            {
                Id = id,
                Nome = nome,
                JsonDataNascimento = dataNascimento,
                Identificacao = identificacao,
                Sexo = sexo,
                Endereco = endereco,
                JsonCreatedAt = createdAt,
                JsonUpdatedAt = updatedAt,
                Url = url
            };
        }

        protected override List<Entity> CreateList(DataTable dt)
        {
            return new List<Entity>((from DataRow dr in dt.Rows

                select new Pessoa
                {
                    Id = !(dr["id"] is DBNull) ? (int?)((long)dr["id"]) : null,
                    Nome = !(dr["nome"] is DBNull) ? dr["nome"].ToString() : null,
                    JsonDataNascimento = !(dr["dataNascimento"] is DBNull) ? dr["dataNascimento"].ToString() : null,
                    Identificacao = !(dr["identificacao"] is DBNull) ? dr["identificacao"].ToString() : null,
                    Sexo = !(dr["sexo"] is DBNull) ? BoolFromLong((long)dr["sexo"]) : null,
                    Endereco = !(dr["endereco"] is DBNull) ? dr["endereco"].ToString() : null,
                    JsonCreatedAt = !(dr["createdAt"] is DBNull) ? dr["createdAt"].ToString() : null,
                    JsonUpdatedAt = !(dr["updatedAt"] is DBNull) ? dr["updatedAt"].ToString() : null,
                    Url = !(dr["url"] is DBNull) ? dr["url"].ToString() : null

                }).ToList());
        }

        private long LongFromBool(bool? bit)
        {
            return bit.Value ? 1 : 0;
        }

        private bool? BoolFromLong(long bit)
        {
            return bit == 0 ? false : true;
        }
    }
}