function buildGraficoIdade(pessoas) {
	var lt10 = 0;
	var lt20 = 0;
	var lt30 = 0;
	var lt40 = 0;
	var mt40 = 0;
	for (pessoa of pessoas) {
		var idade = calcAge(new Date(pessoa.dataNascimento));
		if (idade < 10) {
			lt10++;
		} else if (idade < 20) {
			lt20++;
		} else if (idade < 30) {
			lt30++;
		} else if (idade < 40) {
			lt40++;
		} else {
			mt40++;
		}
	}
	
	var ctx = document.getElementById("graficoIdade").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["0 a 9", "10 a 19", "20 a 29", "30 a 39", "40 ou mais"],
			datasets: [{
				label: 'Número de Pessoas por Faixa Etária',
				data: [lt10, lt20, lt30, lt40, mt40],
				backgroundColor: [
					'rgba(255, 99, 132, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(75, 192, 192, 0.2)',
					'rgba(153, 102, 255, 0.2)'
				],
				borderColor: [
					'rgba(255,99,132,1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(153, 102, 255, 1)'
				],
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			},
			responsive: false
		}
	});

}

function buildGraficoSexo(pessoas) {
	var masc = 0;
	var fem = 0;
	for (pessoa of pessoas) {
		if (pessoa.sexo) {
			masc++;
		} else {
			fem++;
		}
	}
	
	var ctx = document.getElementById("graficoSexo").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Masculino", "Feminino"],
			datasets: [{
				label: 'Número de Pessoas por Sexo',
				data: [masc, fem],
				backgroundColor: [
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 99, 132, 0.2)'
				],
				borderColor: [
					'rgba(54, 162, 235, 1)',
					'rgba(255,99,132,1)'
				],
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			},
			responsive: false
		}
	});

}

function calcAge(birthday) {
	var now = new Date();
	var currentDate = now.getDate();
	var currentMonth = now.getMonth();
	var currentYear = now.getFullYear();
	var birthDate = birthday.getDate();
	var birthMonth = birthday.getMonth();
	var birthYear = birthday.getFullYear();
	
	var age = currentYear - birthYear;
	
	if(currentMonth < birthMonth || (currentMonth == birthMonth && currentDate < birthDate)) age++;
	
	return age;
}