var apiPath = `http://${window.location.hostname}/Rest API/Endpoints/`;

angular.module("PessoaCRUD", [])
	.controller("PessoaController", ['$http', function($http) {
		
		var pessoas = this;
		pessoas.entities;
		pessoas.isEditing = false;
		
		pessoas.renderDate = function(dateString) {
			var parts = dateString.split('-');
			return `${parts[2]}/${parts[1]}/${parts[0]}`;
		}
		
		pessoas.formatDate = function(dateString) {
			var parts = dateString.split('/');
			return `${parts[2]}-${parts[1]}-${parts[0]}`;
		}
		
		pessoas.renderSexo = function(sexo) {
			if(sexo) return "Masculino";
			return "Feminino";
		}
		
		pessoas.dateMask = function() {
			if(pessoas.nascimento.length > 10) {
				pessoas.nascimento = pessoas.nascimento.substr(0,10);
				return;
			}
			
			if(isNaN(pessoas.nascimento[pessoas.nascimento.length - 1])) {
				pessoas.nascimento = pessoas.nascimento.substr(0,pessoas.nascimento.length - 1);
				return;
			}
			
			if(pessoas.nascimento.length == 2 || pessoas.nascimento.length == 5) pessoas.nascimento = pessoas.nascimento + '/';
		}
		
		pessoas.clearFields = function() {
			pessoas.id = "";
				pessoas.nome = "";
				pessoas.nascimento = "";
				pessoas.identificacao = "";
				pessoas.sexo = -1;
				pessoas.endereco = "";
		}
		
		pessoas.validateFields = function() {
			var message = "";
			if (!pessoas.nome) {
				message = "Campo Nome é obrigatório";
			}
			 
			if (!pessoas.nascimento) {
				message += "\nCampo Data de Nascimento é obrigatório";
			} else if (pessoas.nascimento.length < 10) {
				message += "\nFormato de data inválido";
			}
			 
			if (!pessoas.identificacao) {
				message += "\nCampo Documento de Identificação é obrigatório";
			}
			 
			if (pessoas.sexo == undefined || pessoas.sexo < 0) {
				message += "\nCampo Sexo é obrigatório";
			}
			
			if (!pessoas.endereco) {
				message += "\nCampo Endereço é obrigatório";
			}
			
			if (message) alert(message);
			
			return message == "";
		}
		
		pessoas.cancelar = function() {
			pessoas.clearFields();
			pessoas.isEditing = false;
		}
		
		pessoas.listar = function() {
			pessoas.entities = [];
			$http.get(`${apiPath}ListPessoa.ashx`).then(function(response) {
				pessoas.entities = response.data;
				buildGraficoIdade(pessoas.entities);
				buildGraficoSexo(pessoas.entities);
			}, function (response) {
				alert("Houve um erro ao listar pessoas.");
			});
		}
		
		pessoas.editar = function(id) {
			$http.get(`${apiPath}FindPessoa.ashx?id=${id}`).then(function(response) {
				var pessoa = response.data;
				pessoas.id = pessoa.id;
				pessoas.nome = pessoa.nome;
				pessoas.nascimento = pessoas.renderDate(pessoa.dataNascimento);
				pessoas.identificacao = pessoa.identificacao;
				pessoas.sexo = pessoa.sexo ? 1 : 0;
				pessoas.endereco = pessoa.endereco;
				
				pessoas.isEditing = true;
				$('#linkPersistir').click();
			}, function (response) {
				alert("Houve um erro ao buscar pessoa a ser editada.");
			});
		}
		
		pessoas.persistir = function() {
			if(pessoas.validateFields()) {
				var pessoa = {
					id: pessoas.id != "" ? pessoas.id : null,
					nome: pessoas.nome,
					dataNascimento: pessoas.formatDate(pessoas.nascimento),
					identificacao: pessoas.identificacao,
					sexo: new Boolean(pessoas.sexo),
					endereco: pessoas.endereco
				};
				
				var success = true;
			
			
				if(pessoas.isEditing) {
					$http.put(`${apiPath}PersistPessoa.ashx`, JSON.stringify(pessoa)).then(function(response) {
							if(response.data.indexOf("erro") > -1) success = false;
							alert(response.data);
							pessoas.listar();
							$('#linkListar').click();
					}, function(response) {
						alert("Houve um erro ao atualizar pessoa editada. Tente de novo para não perder os dados.");
						success = false;
					});
				} else {
					$http.post(`${apiPath}PersistPessoa.ashx`, JSON.stringify(pessoa)).then(function(response) {
							if(response.data.indexOf("erro") > -1) success = false;
							alert(response.data);
							pessoas.listar();
							$('#linkListar').click();
					}, function(response) {
						alert("Houve um erro ao criar pessoa. Tente de novo para não perder os dados.");
						success = false;
					});
				}
				
				if(success) {
					pessoas.clearFields();
					pessoas.isEditing = false;
				}
			}
		}
		
		pessoas.deletar = function(id) {
			$http.delete(`${apiPath}DeletePessoa.ashx?id=${id}`).then(function(response) {
					alert(response.data);
					pessoas.listar();
			}, function(response) {
				alert("Houve um erro ao deletar pessoa. Por favor tente de novo.");
			});
		}
		
		pessoas.listar();
	}]);